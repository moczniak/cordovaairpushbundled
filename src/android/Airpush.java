package com.moczniak.airpush;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;


import com.evirsosl.wmdrufjz298876.AdConfig;
import com.evirsosl.wmdrufjz298876.AdConfig.AdType;
import com.evirsosl.wmdrufjz298876.AdConfig.EulaLanguage;
import com.evirsosl.wmdrufjz298876.AdView;
import com.evirsosl.wmdrufjz298876.EulaListener;
import com.evirsosl.wmdrufjz298876.AdListener;  
import com.evirsosl.wmdrufjz298876.Main;

public class Airpush extends CordovaPlugin {

    private Main main; //Declare here

    @Override
    public boolean execute(String action, JSONArray data, final CallbackContext callbackContext) throws JSONException {

        if (action.equals("init")) {
            AdConfig.setAppId(Integer.parseInt(data.getString(0)));  //setting appid. 
            AdConfig.setApiKey(data.getString(1)); //setting apikey
            AdConfig.setEulaListener(new EulaListener(){      
                @Override
                public void optinResult(boolean isAccepted) {
                   if (isAccepted) {
                       callbackContext.success("true");
                   }else {
                       callbackContext.success("false");
                   }  
                }
                @Override
                public void showingEula() {
                }
            });
            AdConfig.setCachingEnabled(false); //Enabling SmartWall ad caching. 
            AdConfig.setPlacementId(0); //pass the placement id.
            AdConfig.setEulaLanguage(EulaLanguage.ENGLISH); //Set the eula langauge
            main = new Main(this.cordova.getActivity().getApplicationContext(), null); 
			
			
        }

        if (action.equals("showBanner360")) {
            cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    main.start360BannerAd(cordova.getActivity(), null);   
                }
            });
        }
        if (action.equals("showInterstitial")) {
			main.startInterstitialAd(AdType.smartwall, null);
        }

        return true;
    }
}